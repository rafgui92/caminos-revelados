# Caminos Revelados
======================

Este archivo contiene la informacion de contenido link y uso del repositorio dispuesto a entrega para SED con el fin de que el usuario final pueda modificar, cambiar o respaldar este codigo con el permiso de la SED o el usuario final y el dueño del repositorio.

- [Contenido](#Contenido)
- [Caminos Revelados](#Caminos-Revelados-link)
- [EasyAR](#EasyAR)
- [Politicas de repositorio](#Politicas-de-repositorio)

## Contenido ##

**El repositorio a continuon contiene los siguientes archivos:**

    -   Codigo de UNITY
    -   Politicas de uso aplicacion movil
    -   Archivos APK, AAB, OBB de publicacion de la version 1.1.2
    

## Caminos Revelados link ##

caminos revelados se encuentra oficialmente publicado en la google play store bajo el siguiente link

 https://play.google.com/store/apps/details?id=com.rafgui.caminosrevelados

## EasyAR ##

La motor Realidad Aumentada (AR) por sus siglas en ingles, API desarrollado por la compañia VisionStar Information Technology en shangai, es un motor de AR que a tomado fuerza en los ultimos años desde empresas chinas como empresas como PEPSI, KFC, VOGUE entre otras aplicacion y juegos desarrolladas en este motor para mas informacion sobre [EasyAR](https://www.easyar.com).

La informacion brindada para el uso del API es la siguiente:

### SDK License Key
    zQVyNskWairRd9k8nNJ1LspEhzKZi5N+6LFaTf03RB3JJ0IA/SpVTbJmUw7uI1QG/yFDHOEwRC/vKUAG5GpCAOVmDU3lJVIb7TZqCvENRU2ydQ1N5C1CCuY3RByqfnoUqiZUAewoRCbsNwNV02ZCAOVqUw7uI1QGpidAAuEqThz6IVcK5CVFAPtmDU3rK0xB+iVHCP0tDwzpKUgB5zdTCv4hTQ7sK1JN1WgDGek2SA7mMFJNsh8DDek3SAyqGQ1N+ChAG+4rUwL7Zhs0qjNIAewrVhyqaAMC6ScDMqRmRBf4LVMK3C1MCtswQAL4ZhsB/ShNQ6otUiPnJ0ADqn5HDuQ3RBKkPwMN/SpFA+0NRRyqfnpN6ytMQfolRwj9LQ8M6SlIAec3Uwr+IU0O7CtSTdVoAxnpNkgO5jBSTbIfAw3pN0gMqhkNTfgoQBvuK1MC+2YbNKolTwv6K0gLqhkNTe08UQb6IXUG5SFyG+kpUU2yKlQD5GgDBvsITgzpKANV7iVNHO05DRSqJlQB7ChEJuw3A1XTZkIA5WpTDu4jVAamJ0AC4SpOHPohVwrkJUUA+2Z8Q6oyQB3hJU8b+2YbNKomQBzhJwMypGZRA+kwRwD6KVJNsh8DBuc3AzKkZkQX+C1TCtwtTArbMEAC+GYbAf0oTUOqLVIj5ydAA6p+Rw7kN0QS1Tk2ScL90UMQKgIYYuJZPukwvp0jrWmoe8rB3N6tEe5htxFLoC7Z2LtqMzUzrOTSEwcfwdsWa3wDr2g8TO+U2rp2ptzWstF28ISNQ/qJiyVB+DSJyZP5alx3ZxkOm7EISiIjo+01FpJYeyeZ533F8c1Vlxt/Fb5n4xV6+j8yuUe2nUGWrhyE8D2pIlkacFjp/454kKaz6Oax8Gn82vkovTehCeBtepeZ+xeJz7ANbX7mCir445IAaofF81jTqnJdcSNSNVxQd+K5kyOikJQSsvEFLHqB9zqiju3dJSwbjbXXBvF4xrQz5BjNEVeicRUV9Cr/IItKEKiTk5rb8hSIRCFv 

### Bundle ID

| **IOS** : com.rafgui.caminosrevelados |
| **Android** : com.rafgui.caminosrevelados |

## Politicas de repositorio

Esta REPOSITORIO se entrega en mutuo acuerdo de satisfacción, si por algún motivo no se llega a este acuerdo de satisfaccion este código le pertenece, para uso y comercializacion del desarrollador de la app o dueño del repositorio.

